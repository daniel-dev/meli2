//
//  MeLi2UITests.m
//  MeLi2UITests
//
//  Created by Daniel Rodriguez on 18-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MeLi2UITests : XCTestCase

@end

@implementation MeLi2UITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    XCUIApplication *app2 = [[XCUIApplication alloc] init];
    
    XCUIApplication *app = app2;
    [app.buttons[@"INICIAR"] tap];
    [app.textFields[@"Monto"] tap];
    
 //   XCUIApplication *app2 = app;
    [app2/*@START_MENU_TOKEN@*/.keys[@"2"]/*[[".keyboards.keys[@\"2\"]",".keys[@\"2\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    
    XCUIElement *key = app2/*@START_MENU_TOKEN@*/.keys[@"5"]/*[[".keyboards.keys[@\"5\"]",".keys[@\"5\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [key tap];
    [key tap];
    
    XCUIElement *key2 = app2/*@START_MENU_TOKEN@*/.keys[@"0"]/*[[".keyboards.keys[@\"0\"]",".keys[@\"0\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [key2 tap];
    [key2 tap];
    [key2 tap];
    
    XCUIElement *deleteKey = app2/*@START_MENU_TOKEN@*/.keys[@"Delete"]/*[[".keyboards.keys[@\"Delete\"]",".keys[@\"Delete\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/;
    [deleteKey tap];
    [deleteKey tap];
    [app.buttons[@"SIGUIENTE"] tap];
    
    XCUIElementQuery *tablesQuery = app2.tables;
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Visa"]/*[[".cells.staticTexts[@\"Visa\"]",".staticTexts[@\"Visa\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [tablesQuery/*@START_MENU_TOKEN@*/.staticTexts[@"Tarjeta Shopping"]/*[[".cells.staticTexts[@\"Tarjeta Shopping\"]",".staticTexts[@\"Tarjeta Shopping\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
    [app.buttons[@"FINALIZAR"] tap];
    //[app.buttons[@"Cerrar"] tap];

}



@end
