//
//  UIViewController+Alert.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)


- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message acceptBlock:(void(^)(void))block;


@end
