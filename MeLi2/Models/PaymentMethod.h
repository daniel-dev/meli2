//
//  PaymentMethod.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PaymentMethod : NSObject

@property (nonatomic,strong) NSString   *paymentName;
@property (nonatomic,strong) NSString   *thumbnail;
@property (nonatomic,strong) NSString   *idMp;
@property (nonatomic,strong) UIImage    *thumbnailImage;
@property (nonatomic,strong) NSNumber   *max_allowed_amount;

@end
