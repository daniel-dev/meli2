//
//  Bank.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Bank : NSObject

@property (nonatomic,strong) NSString   *name;
@property (nonatomic,strong) NSString   *thumbnail;
@property (nonatomic,strong) NSString   *idBank;
@property (nonatomic,strong) UIImage    *thumbnailImage;

@end
