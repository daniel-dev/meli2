//
//  SharePayment.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharePayment : NSObject

@property (nonatomic,strong) NSString   *recommended_message;

@end
