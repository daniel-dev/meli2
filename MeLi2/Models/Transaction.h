//
//  Transaction.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "Bank.h"
#import "SharePayment.h"

@interface Transaction : NSObject

@property (nonatomic,strong) NSNumber       *transactionAmount;
@property (nonatomic,strong) PaymentMethod  *paymentMethod;
@property (nonatomic,strong) Bank           *bank;
@property (nonatomic,strong) SharePayment   *sharedPayment;

@end
