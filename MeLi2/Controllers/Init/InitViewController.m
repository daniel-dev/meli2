//
//  ViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 13-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "InitViewController.h"


@interface InitViewController ()

@end

@implementation InitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [EepLog InfoLog:@"App Iniciando"];
    
    // Revisamos ConfHelper
    ConfigHelper *confHelper = [ConfigHelper defaultMercadoPagoConfig];
    
    [EepLog FutureLog:@"Debería llegar: https://api.mercadopago.com/v1/ y 444a9ef5‐8a6b‐429f‐abdf‐587639155d88"];
    [EepLog InfoLog:@"En Efecto llego: %@ y %@",confHelper.MPBaseUrl,confHelper.clientKey];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
