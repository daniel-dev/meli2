//
//  StepOneViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 13-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "StepOneViewController.h"
#import "ReviewViewController.h"


@interface StepOneViewController ()

@property (nonatomic) NSNumber *montoIngresado;
@property (nonatomic) NSNumberFormatter *currencyFormatter;
@property (nonatomic) NSMutableString *inpStrAmount;


@end

@implementation StepOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _inpStrAmount = [[NSMutableString alloc] init];
    _currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyAccountingStyle];
    [_currencyFormatter setMaximumFractionDigits:0];
    [self.navigationController setDelegate:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
    
    Transaction *transaction = [Transaction new];
    [transaction setTransactionAmount:_montoIngresado];
    
    StepTwoViewController *stvc = (StepTwoViewController*)[segue destinationViewController];
    [stvc setTransaction:transaction];

    
    return;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    return [self isAccepted:_inNumAmount];
}

- (BOOL)isAccepted:(UITextField*)textField{
    
    //Limpiar espacios en blanco y saltos de linea
    NSString *enterValue = [[textField text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *msg = nil;
    //revisar que enterValue NO es Nil
    if (!enterValue) {
        [EepLog WarnLog:@"enterValue es Nil"];
        return NO;
    }
    //Revisar que largo sea mayor que 0
    if ([enterValue length] == 0) {
        [EepLog WarnLog:@"NO se ha ingresado monto"];
        msg = @"Debes Ingresar un Monto";
        [self showAlertWithTitle:@"Error" message:msg acceptBlock:nil];
        return NO;
    }
    
    //Revisar que solo sea numeros
    NSCharacterSet *letras = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([enterValue rangeOfCharacterFromSet:letras].location != NSNotFound) {
        [EepLog WarnLog:@"Hay Letras"];
        msg = @"Deben ser solo numeros";
        [self showAlertWithTitle:@"Error" message:msg acceptBlock:nil];

        return NO;
    }
    
    //Ya que son numeros, lo volvemos Numero. Entero?
    _montoIngresado = [NSNumber numberWithInt:[enterValue intValue]];
    if (!_montoIngresado){
        [EepLog WarnLog:@"No se ha podido transformar enterValue a NSNumber"];
        msg = @"No se ha podido transformar el numero";
        [self showAlertWithTitle:@"Error" message:msg acceptBlock:nil];
        return NO;
    }
    [EepLog InfoLog:@"Monto Ingrsado es: %d",[_montoIngresado intValue]];
    
    //Finalmente revisamos que no sea 0
    if ([_montoIngresado intValue] == 0) {
        [EepLog WarnLog:@"Monto Ingresado es 0"];
        msg = @"El Monto debe ser superior a 0";
        [self showAlertWithTitle:@"Error" message:msg acceptBlock:nil];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)unwindToStepOne:(UIStoryboardSegue*)segue{
    
    
}

- (void)finishProcess:(Transaction*)transaction{
    [EepLog InfoLog:@"finishProcess %@",transaction];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 +  NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

    });
    dispatch_async(dispatch_get_main_queue(), ^{
       
    });
   
    
    /*
    
    UIPopoverPresentationController *poppc = [[UIPopoverPresentationController alloc] initWithPresentedViewController:rvc presentingViewController:self];
    [poppc setDelegate:self];
    */
    
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{

    return UIModalPresentationNone;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
    if(viewController == self && _transaction != nil){
        [_inNumAmount setText:@""];
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            ReviewViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
            [rvc setTransaction:self->_transaction];
            [rvc setModalPresentationStyle:UIModalPresentationPopover];
            UIPopoverPresentationController *pop = rvc.popoverPresentationController;
            [pop setSourceRect:self.navigationController.navigationBar.frame];
            
            [pop setPermittedArrowDirections:UIPopoverArrowDirectionAny];
            [pop setSourceView:self.view];
            [pop setDelegate:self];
            [self presentViewController:rvc animated:YES completion:nil];
            self->_transaction = nil;
        });
        
    }
    
}



@end
