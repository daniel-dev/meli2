//
//  StepOneViewController.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 13-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import "UIViewController+Alert.h"
#import "StepTwoViewController.h"


@interface StepOneViewController : UIViewController<UITextFieldDelegate,UIPopoverPresentationControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic) Transaction *transaction;
@property (weak, nonatomic) IBOutlet UITextField *inNumAmount;

- (void)finishProcess:(Transaction*)transaction;

@end
