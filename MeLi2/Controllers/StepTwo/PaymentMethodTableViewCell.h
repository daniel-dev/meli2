//
//  PaymentMethodTableViewCell.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentMethodTableViewCell : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *name;
@property (nonatomic,weak) IBOutlet UIImageView *thumbnail;
@end
