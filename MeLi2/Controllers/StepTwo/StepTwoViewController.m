//
//  StepTwoViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "StepTwoViewController.h"
#import "ConfigHelper.h"
#import "PaymentMethodTableViewCell.h"
#import "LoadingTableViewCell.h"
#import "ImageHelper.h"
#import "StepThreeViewController.h"

@interface StepTwoViewController ()

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic,strong) NSArray *paymentMethodList;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadingList;
@end

@implementation StepTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _paymentMethodList = [NSArray array];
    _imageDownloadingList = [NSMutableDictionary new];
    NSURL *urlPaymentMethods = [ConfigHelper urlForService:@"payment_methods" WithParams:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self startDownloadingDataForUrl:urlPaymentMethods];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startDownloadingDataForUrl:(NSURL*)url{
    [EepLog InfoLog:@"%s %@",__PRETTY_FUNCTION__,url];
    BOOL isInternet = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (isInternet == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        //[self mostrarAlertConTitulo:@"Ops!" mensaje:@"No Hay Conexión" bloqueFinal:^{
        [self showAlertWithTitle:@"Error" message:@"No hay conexión" acceptBlock:nil];
        return;
        
    }
    
    ServiceHelper *service = [[ServiceHelper alloc] init];
    [service getArrayOf:PaymentMethod.class withUrl:url completed:^(NSArray *list) {
        if(list){
            self->_paymentMethodList = list;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_table reloadData];
            });
            
        }else{
            
            [self showAlertWithTitle:@"Error" message:@"Ha ocurrido un errror al obtener medio de pago" acceptBlock:^{
                
            }];
        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    [EepLog InfoLog:@"%s",__PRETTY_FUNCTION__];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Si la lista es 0 entonces, colocar un row para el loader
 
    return [_paymentMethodList count]>0 ? [_paymentMethodList count]: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_paymentMethodList.count == 0) {
        LoadingTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"LoadingTableViewCell" forIndexPath:indexPath];
        return celda;
        
    }
    PaymentMethod *mp = [_paymentMethodList objectAtIndex:indexPath.row];
    PaymentMethodTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"PaymentMethodTableViewCell"];
    [celda.name setText:[mp paymentName]];
    
    if(!mp.thumbnailImage){
        [self startDownladingImageFor:mp paraIndexPath:indexPath];
        [EepLog WarnLog:@"Descargar %@",mp.thumbnail];

    }else{
        [celda.thumbnail setImage:mp.thumbnailImage];
    }
    return celda;
    
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PaymentMethod *paymentMethod = [_paymentMethodList objectAtIndex:indexPath.row];
    
    int monto = [[_transaction transactionAmount] intValue];
    int max = [[paymentMethod max_allowed_amount] intValue];
    if(monto > max){
        [EepLog WarnLog:@"Monto supera máximo!"];
        NSString *msj = [NSString stringWithFormat:@"Este Medio de Pago solo soporta hasta %d",max];
        [self showAlertWithTitle:@"Error" message:msj acceptBlock:nil];
        
        return;
    }
    
    [_transaction setPaymentMethod:paymentMethod];
    [EepLog HIDLog:@"%s %@",__PRETTY_FUNCTION__,paymentMethod.paymentName];
    
    StepThreeViewController *stvc = [self.storyboard instantiateViewControllerWithIdentifier:@"StepThreeViewController"];
    [stvc setTransaction:_transaction];
    [self.navigationController pushViewController:stvc animated:YES];
    
    
}

- (void)startDownladingImageFor:(PaymentMethod*)medioPago paraIndexPath:(NSIndexPath*)indexPath{
    
    ImageHelper *imgHelper = (self.imageDownloadingList)[indexPath];
    if(!imgHelper){
        imgHelper = [[ImageHelper alloc] init];
        [imgHelper setCompletedBlock:^{
         
            
            dispatch_async(dispatch_get_main_queue(), ^{
              
                PaymentMethodTableViewCell *celda = (PaymentMethodTableViewCell*)[self->_table cellForRowAtIndexPath:indexPath];
                [celda.thumbnail setImage:medioPago.thumbnailImage];
             
            });
            
        }];
        
        (self.imageDownloadingList)[indexPath] = imgHelper;
        [imgHelper startDownloadingFor:medioPago];
    }
}

- (void)cancellAllDownloads{
    NSArray *allDownloads = [self.imageDownloadingList allValues];
     [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
     [self.imageDownloadingList removeAllObjects];
}

@end
