//
//  StepTwoViewController.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "ServiceHelper.h"
#import "Transaction.h"
#import "UIViewController+Alert.h"
#import "LoadingTableViewCell.h"
@interface StepTwoViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic) Transaction *transaction;

@end
