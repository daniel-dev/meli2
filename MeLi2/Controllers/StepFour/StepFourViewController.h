//
//  StepFourViewController.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "UIViewController+Alert.h"
#import "SharePayment.h"
#import "ServiceHelper.h"
#import "ConfigHelper.h"

@interface StepFourViewController : UIViewController<UIPickerViewDelegate>

@property (nonatomic) Transaction *transaction;

@end
