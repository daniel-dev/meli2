//
//  StepFourViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "StepFourViewController.h"
#import "StepOneViewController.h"

@interface StepFourViewController ()

@property (nonatomic,strong)NSArray *listOfAvailableSP;
@property (weak, nonatomic) IBOutlet UIPickerView *spPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnFinish;


@end

@implementation StepFourViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _listOfAvailableSP = [NSArray array];
    
    NSURL *url = [ConfigHelper urlForServices:@"payment_methods" withMethod:@"installments" withParameters:@{@"payment_method_id":_transaction.paymentMethod.idMp,@"issuer.id":_transaction.bank.idBank,@"amount":_transaction.transactionAmount}];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self startDownloadingDataForUrl:url];
    });
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)startDownloadingDataForUrl:(NSURL*)url{
    
    [EepLog FutureLog:@"%s %@",__PRETTY_FUNCTION__,url];
    BOOL intenert = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (intenert == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        [self showAlertWithTitle:@"Error" message:@"No hay conexion" acceptBlock:nil];
        return;
    }
    
    ServiceHelper *service = [[ServiceHelper alloc] init];
    
    [service getArrayOf:SharePayment.class withUrl:url completed:^(NSArray *list) {
        
        if(list){
            if([list count] == 0){
                [self showAlertWithTitle:@"Error" message:@"No se han encontrado cuotas para este tipo de pago" acceptBlock:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    self->_listOfAvailableSP = list;
                    [self->_spPicker reloadAllComponents];
                    int aSeleccionar = floor(list.count/2);
                    [self->_spPicker selectRow:aSeleccionar inComponent:0 animated:YES];
                    [_btnFinish setUserInteractionEnabled:YES];
                    
                });
            }
            
        }else{
            
            
            [self showAlertWithTitle:@"Error" message:@"No se han encontrado lista de cuotas" acceptBlock:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            
        }
    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[StepOneViewController class]]) {
        StepOneViewController *sovc = (StepOneViewController*)[segue destinationViewController];
        [sovc setTransaction:_transaction];
        //[sovc finishProcess:_transaction];
    }
    
}

#pragma mark - PickerView
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
    
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return _listOfAvailableSP.count >0 ? _listOfAvailableSP.count: 1;
}


- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    [EepLog FutureLog:@"%s",__PRETTY_FUNCTION__];
    UILabel *vista = (UILabel*)view;
    if(!vista){
        
        vista = [[UILabel alloc] init];
        [vista setTextAlignment:NSTextAlignmentCenter];
    }
    if(_listOfAvailableSP.count == 0){
        [vista setText:@"Cargando..."];
    }else{
        SharePayment *sPayment = [_listOfAvailableSP objectAtIndex:row];
        [vista setText:sPayment.recommended_message];
    }
    
    return vista;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [EepLog HIDLog:@"%s",__PRETTY_FUNCTION__];
}

- (IBAction)finishProcess:(id)sender {
    
    [EepLog HIDLog:@"Finalizar"];
    [EepLog HIDLog:@"%d",[_spPicker selectedRowInComponent:0]];

    SharePayment *sPayment = [_listOfAvailableSP objectAtIndex:[_spPicker selectedRowInComponent:0]];
    
    [_transaction setSharedPayment:sPayment];
    
    [self performSegueWithIdentifier:@"UnwindToStepOne" sender:self];
    
}


@end
