//
//  StepThreeViewController.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "UIViewController+Alert.h"
#import "ServiceHelper.h"
#import "ConfigHelper.h"
#import "Bank.h"
#import "ImageHelper.h"
#import "LoadingTableViewCell.h"
#import "BankTableViewCell.h"
#import "StepFourViewController.h"

@interface StepThreeViewController : UIViewController

@property (nonatomic) Transaction *transaction;

@end
