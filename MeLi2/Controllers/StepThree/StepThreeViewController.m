//
//  StepThreeViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "StepThreeViewController.h"

@interface StepThreeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic) NSArray *bankList;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadingList;

@end

@implementation StepThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [EepLog InfoLog:@"%@",_transaction.paymentMethod.paymentName];
    
    //NSURL *url = [ConfigHelper urlForService:@"" WithParams:@{@"payment_method_id":_transaction.paymentMethod.idMp}];
    NSURL *url = [ConfigHelper urlForServices:@"payment_methods" withMethod:@"card_issuers" withParameters:@{@"payment_method_id":_transaction.paymentMethod.idMp}];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self startDownloadingDataForUrl:url];
    });
    
    
}
- (void)startDownloadingDataForUrl:(NSURL*)url{
    
    [EepLog InfoLog:@"%s %@",__PRETTY_FUNCTION__,url];
    
    BOOL intenert = [[Reachability reachabilityWithHostName:@"api.mercadopago.com"] currentReachabilityStatus];
    if (intenert == NotReachable) {
        [EepLog WarnLog:@"No Hay Conexión"];
        [self showAlertWithTitle:@"Error" message:@"No hay conexion" acceptBlock:nil];
        return;
    }
    
    
    ServiceHelper *service = [[ServiceHelper alloc] init];
    
    [service getArrayOf:Bank.class withUrl:url completed:^(NSArray *list) {
       
        if(list){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([list count]>0) {
                    self->_bankList = list;
                    [self->_table reloadData];
                }else{
                    [self showAlertWithTitle:@"Error" message:@"No Hay Bancos para este tipo" acceptBlock:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                }
                
            });
        }else{
            
            [self showAlertWithTitle:@"Error" message:@"Ha ocurrido un error al obtener data" acceptBlock:nil];
        }
        
        
    }];
    
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Si la lista es 0 entonces, colocar un row para el loader
    return [_bankList count]>0 ? [_bankList count]: 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_bankList.count == 0) {
        LoadingTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"LoadingTableViewCell" forIndexPath:indexPath];
        return celda;
        
    }

    Bank *bank = [_bankList objectAtIndex:indexPath.row];
    BankTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"BankTableViewCell"];
    [celda.name setText:[bank name]];
    
    if(!bank.thumbnailImage){
         [self startDownladingImageFor:bank paraIndexPath:indexPath];
    }else{
        [celda.thumbnail setImage:bank.thumbnailImage];
    }
    
    
    return celda;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Bank *bank = [_bankList objectAtIndex:indexPath.row];
    [EepLog HIDLog:@"%s %@",__PRETTY_FUNCTION__,bank.name];
    [_transaction setBank:bank];
    
    StepFourViewController *sfvc = [self.storyboard instantiateViewControllerWithIdentifier:@"StepFourViewController"];
    [sfvc setTransaction:_transaction];
    [self.navigationController pushViewController:sfvc animated:YES];
    
}


- (void)startDownladingImageFor:(Bank*)banco paraIndexPath:(NSIndexPath*)indexPath{
    
    ImageHelper *imgHelper = (self.imageDownloadingList)[indexPath];
    if(!imgHelper){
        imgHelper = [[ImageHelper alloc] init];
        [imgHelper setCompletedBlock:^{
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                BankTableViewCell *celda = (BankTableViewCell*)[self->_table cellForRowAtIndexPath:indexPath];
                [celda.thumbnail setImage:banco.thumbnailImage];
                
            });
            
        }];
        
        (self.imageDownloadingList)[indexPath] = imgHelper;
        [imgHelper startDownloadingFor:banco];
    }
}

- (void)cancellAllDownloads{
    NSArray *allDownloads = [self.imageDownloadingList allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
    [self.imageDownloadingList removeAllObjects];
}


    
@end
