//
//  ReviewViewController.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 15-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "ReviewViewController.h"

@interface ReviewViewController ()


@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblBank;
@property (weak, nonatomic) IBOutlet UILabel *lblSP;

@property (nonatomic) NSNumberFormatter *currencyFormatter;


@end

@implementation ReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyAccountingStyle];
    [_currencyFormatter setMaximumFractionDigits:0];
    
    
    Bank *bank                      = _transaction.bank;
    PaymentMethod *paymentMethod    = _transaction.paymentMethod;
    SharePayment *sharedPayment     = _transaction.sharedPayment;
    NSNumber *amount                = _transaction.transactionAmount;
    
    [_lblAmount setText:[_currencyFormatter stringFromNumber:amount]];
    [_lblBank setText:[bank name]];
    [_lblPaymentMethod setText:[paymentMethod paymentName]];
    [_lblSP setText:[sharedPayment recommended_message]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeReview:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
