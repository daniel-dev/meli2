//
//  ReviewViewController.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 15-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Transaction.h"
#import "Bank.h"
#import "SharePayment.h"
#import "PaymentMethod.h"

@interface ReviewViewController : UIViewController

@property (nonatomic,weak) Transaction *transaction;

@end
