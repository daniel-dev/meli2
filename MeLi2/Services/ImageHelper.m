//
//  ImageHelper.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "ImageHelper.h"

@implementation ImageHelper

- (void)startDownloadingFor:(id)object{
    [EepLog InfoLog:@"--> %s",__PRETTY_FUNCTION__];
    
    @try{
        if ([object isKindOfClass:Bank.class]) {
            NSURLRequest *rquest = [NSURLRequest requestWithURL:[NSURL URLWithString:[(Bank*)object thumbnail]]];
            //iniciamos sessionTask
            _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:rquest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error) {
                    //si hay error mostrar error y setear imagen a placeholder
                    [EepLog ErrorLog:@"Error al Descargar Imagen"];
                    
                    [(Bank*)object setThumbnailImage:[UIImage imageNamed:@"placeholder"]];
                    
                }else{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if(httpResponse.statusCode !=200){
                        [(Bank*)object setThumbnailImage:[UIImage imageNamed:@"placeholder"]];
                        return;
                    }
                    //si NO hay herror
                    [EepLog InfoLog:@"Eeep"];
                    NSString *e = [(Bank*)object thumbnail];
                    //Iniciamos una cola con bloque
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        //Obtenemos el primer Byte de la Data
                        NSData *byte = [data subdataWithRange:NSMakeRange(0, 1)];
                        Byte *byteData = (Byte*)malloc(1);
                        memcpy(byteData,[byte bytes],1);
                        
                        [EepLog InfoLog:@"Primer Byte de imagen %X para %@",byteData[0],e];
                        
                        //Creamos imagen con Data
                        UIImage *image = [[UIImage alloc] initWithData:data];
                        //Si el primer byte es dsitinto a 0x47, no es el tipo de imagen que queremos, o no es imagen.
                        if (byteData[0]==0x3C) {
                            image = [UIImage imageNamed:@"placeholder"];
                            [EepLog WarnLog:@"Econtro Data, pero NO es imagen"];
                        }
                        
                        [(Bank*)object setThumbnailImage:image]; //imagenThumbnail = image;
                        
                        if(self.completedBlock){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.completedBlock();
                            });
                            
                        }
                    }];
                }
                
            }];
            //se "corre" sessionTask
            [_sessionTask resume];
        }
        
        
        if ([object isKindOfClass:PaymentMethod.class]) {
            NSURLRequest *rquest = [NSURLRequest requestWithURL:[NSURL URLWithString:[(PaymentMethod*)object thumbnail]]];
            //iniciamos sessionTask
            _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:rquest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error) {
                    //si hay error mostrar error y setear imagen a placeholder
                    [EepLog ErrorLog:@"Error al Descargar Imagen"];
                    
                    [(PaymentMethod*)object setThumbnailImage:[UIImage imageNamed:@"placeholder"]];
                     //setImagenThumbnail:[UIImage imageNamed:@"placeholder"]];
                    
                }else{
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    if(httpResponse.statusCode !=200){
                        [(PaymentMethod*)object setThumbnailImage:[UIImage imageNamed:@"placeholder"]];
                        return;
                    }
                    //si NO hay herror
                    [EepLog InfoLog:@"Eeep"];
                    NSString *e = [(PaymentMethod*)object thumbnail];
                    
                    //Iniciamos una cola con bloque
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        
                        //Obtenemos el primer Byte de la Data
                        NSData *byte = [data subdataWithRange:NSMakeRange(0, 1)];
                        Byte *byteData = (Byte*)malloc(1);
                        memcpy(byteData,[byte bytes],1);
                        
                        [EepLog InfoLog:@"Primer Byte de imagen %X para %@",byteData[0],e];
                        
                        //Creamos imagen con Data
                        UIImage *image = [[UIImage alloc] initWithData:data];
                        //Si el primer byte es dsitinto a 0x47, no es el tipo de imagen que queremos, o no es imagen.
                        if (byteData[0]==0x3C) {
                            image = [UIImage imageNamed:@"placeholder"];
                            [EepLog WarnLog:@"Econtro Data, pero NO es imagen"];
                        }
                        [EepLog InfoLog:@"Es iamgen y se descargo"];
                        //se setea la imagen del objecto a la imagen creada
                        [(PaymentMethod*)object setThumbnailImage:image]; //imagenThumbnail = image;
                        
                        //si esxiste el bloque bloqueCompleto, se invoca
                        if(self.completedBlock){
                            //no aseguramos de llamarlo en el main_queue
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //self->completedBlock();
                                [EepLog InfoLog:@"Img en obt %d",CGImageGetHeight(image.CGImage)];
                                self.completedBlock();
                            });
                            
                        }
                    }];
                }
                
            }];
            //se "corre" sessionTask
            [_sessionTask resume];
        }
        
        
        
    }@catch(NSException *exception){
        [EepLog ErrorLog:@"Error! %@",exception.description];
    }
}
- (void)cancelDownload{
    [self.sessionTask cancel];
    _sessionTask = nil;
}
@end
