//
//  ServiceHelper.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "ServiceHelper.h"
#import "PaymentMethod.h"
#import "Bank.h"
#import "SharePayment.h"


@implementation ServiceHelper

- (void)getArrayOf:(id)type withUrl:(NSURL*)url completed:(void(^)(NSArray* list))block{
    
    //Obtenemos un NSURLSessionConfiguration por defecto
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    //Creamos una session, con delegado self y nueva cola de operacion
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:[NSOperationQueue new]];
    //Creamos un NSURLSessionDataTask con la URL
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error){
            //Si hay error entonces informar y enviar bloque final con nil
            [EepLog ErrorLog:@"Error en ServiceHelper %s dataTaskCompletionHandler",__PRETTY_FUNCTION__];
            [EepLog ErrorLog:@"%@",error.description];
            if (block) {
                block(nil);
            }
        }else{
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //revisamos status code
            
            if(httpResponse.statusCode != 200){
                if(block){
                    block(nil);
                }
                
                return;
            }
            
            //Si NO hay error
            NSError *obtenerMediosDePagoError; //Error para Serialización de JSON
            //Transformamos la respuesta a Array
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&obtenerMediosDePagoError];
            
            if(!obtenerMediosDePagoError){
                [EepLog FutureLog:@"Se obtuvo JSON"];
                @try{
                    //Yay Tenemos JSON
                    
                    //Si es tipo MedioPago
                    if ([type isEqual:PaymentMethod.class]) {
                        [EepLog InfoLog:@"Es MedioPago"];
                        //Creamos un NSMutableArray para guardar los Medio de Pago
                        NSMutableArray *listaDeMediosDePago = [[NSMutableArray alloc] init];
                        //Para cada NSDictionary en el array json
                        for (NSDictionary *dk in json) {
                            
                            
                            //seteamos los valores del array, si no existen, ponemos por defecto
                            NSString *name =  [dk valueForKey:@"name"]? [dk valueForKey:@"name"] : @"NO_NAME";
                            NSString *thumbnail =  [dk valueForKey:@"secure_thumbnail"]? [dk valueForKey:@"thumbnail"] : @"NO_THUMB";
                            NSString *idMp =  [dk valueForKey:@"id"]? [dk valueForKey:@"id"] : @"NO_ID";
                            
                            NSNumber *maxAmount = [dk valueForKeyPath:@"max_allowed_amount"]? [NSNumber numberWithInt:[[dk valueForKeyPath:@"max_allowed_amount"]intValue]] : [NSNumber numberWithInt:0];
                            
                            //si los datos estan bien, entonces  creamos y guardamos Medios de Pago
                            if(![name isEqualToString:@"NO_NAME"]){
                                //Creamos una instancia de medio de pago
                                PaymentMethod *paymentMethod = [PaymentMethod new];
                                [paymentMethod setPaymentName:name];
                                [paymentMethod setThumbnail:thumbnail];
                                [paymentMethod setIdMp:idMp];
                                [paymentMethod setMax_allowed_amount:maxAmount];
                                //agregamos el objecto al arreglo
                                [listaDeMediosDePago addObject:paymentMethod];
                            }
                            
                        }//fin for
                        //despues del for, deberiamos tener un arreglo con algunos medios de pago
                        //se invoca al finalBlock con el arreglo
                        block(listaDeMediosDePago);
                    }
                    //fin If MedioPago.class
                    if([type isEqual:Bank.class]){
                        [EepLog InfoLog:@"Es Financiera"];
                        NSMutableArray *listaDeFinancieras = [[NSMutableArray alloc] init];
                        for (NSDictionary *dk in json) {
                            
                            
                            Bank *bank = [Bank new];
                            NSString *name =  [dk valueForKey:@"name"]? [dk valueForKey:@"name"] : @"NO_NAME";
                            NSString *thumbnail =  [dk valueForKey:@"secure_thumbnail"]? [dk valueForKey:@"thumbnail"] : @"NO_THUMB";
                            NSString *idBank =  [dk valueForKey:@"id"]? [dk valueForKey:@"id"] : @"NO_ID";
                            
                            [bank setName:name];
                            [bank setThumbnail:thumbnail];
                            [bank setIdBank:idBank];
                            
                            [listaDeFinancieras addObject:bank];
                        }//fin for
                        block(listaDeFinancieras);
                    }
                    //Fin tipo Financiera
                    //Si es tipo Cuota
                    if ([type isEqual:SharePayment.class]) {
                        [EepLog InfoLog:@"Es Cuota"];
                        NSMutableArray *listaDeCuotas = [[NSMutableArray alloc] init];
                        for (NSDictionary *dk in json) {
                            
                            [EepLog InfoLog:@"%@",[dk valueForKeyPath:@"payer_costs.recommended_message"]];
                            
                            
                            NSArray *recommended_messageArray =  [dk valueForKeyPath:@"payer_costs.recommended_message"]? [dk valueForKeyPath:@"payer_costs.recommended_message"] : @"NO_STR";
                            for (NSString *messageCuota in recommended_messageArray) {
                                [EepLog FutureLog:@"%@",messageCuota];
                                SharePayment *sharePayment = [SharePayment new];
                                [sharePayment setRecommended_message:messageCuota];
                                [listaDeCuotas addObject:sharePayment];
                            }
                            
                            
                        }//fin for
                        block(listaDeCuotas);
                    }
                    
                    
                    
                    
                }
                @catch(NSException *ex){
                    
                }
                
                
            }else{
                [EepLog WarnLog:@"No se obtuvo JSON"];
                if(block){
                    block(nil);
                }
            }
            
            
        }//fin else
    }];
    
    [dataTask resume];
}

@end
