//
//  ServiceHelper.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceHelper : NSObject <NSURLSessionDelegate>


- (void)getArrayOf:(id)type withUrl:(NSURL*)url completed:(void(^)(NSArray* list))block;


@end
