//
//  ImageHelper.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 14-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentMethod.h"
#import "Bank.h"
#import "SharePayment.h"

@interface ImageHelper : NSObject

@property (nonatomic,strong) NSURLSessionDataTask *sessionTask;

@property (nonatomic,copy) void (^completedBlock)(void);

- (void)startDownloadingFor:(id)object;

- (void)cancelDownload;


@end
