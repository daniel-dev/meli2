//
//  ConfigHelper.h
//  MeLi2
//
//  Created by Daniel Rodriguez on 13-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigHelper : NSObject

@property (nonatomic,strong) NSString *MPBaseUrl;
@property (nonatomic,strong) NSString *clientKey;

/*  defaultMercadoPagoConfig obtiene un singleton, lee el archivo AppConfig.plist para obtener
 *   La URL Base de los servicios y la llave de Cliente
 */
/*!
 @discussion Metodo para obtener instancia con valores por defecto, leyendo AppConfig.plist
 @return Instancia ConfHelper con valores por defecto
 */
+ (ConfigHelper*)defaultMercadoPagoConfig;
/*!
 @discussion Metodo para obtener Ruta de servicio con un servicio especifico y parametros
 
 @param service NSString con el nombre del servicio solictado
 @param parameters NSDictionary Diccionario con los parametros a pasar en la URL
 @return NSURL con la URL para consumir
 */
+ (NSURL*)urlForService:(NSString*)service WithParams:(NSDictionary*)parameters;
/*!
 @discussion Metodo para obtener la Ruta de un servicio con un metodo especifico y paramtros
 
 @param service NSString nombre del servicio
 @param method NSString nombre del metodo que se deasea invocar
 @param parameters NSDictionary Diccionario con los parametros necesarios
 @return NSURL con la URL para consumir
 @code [ConfHelper urlParaServicio:@"servicio" conMetodo:@"metodo" ConParametros:@{@"llave":@"valor"}];
 */


+ (NSURL*)urlForServices:(NSString*)service withMethod:(NSString*)method withParameters:(NSDictionary*)parameters;

@end
