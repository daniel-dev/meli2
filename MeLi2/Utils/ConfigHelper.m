//
//  ConfigHelper.m
//  MeLi2
//
//  Created by Daniel Rodriguez on 13-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import "ConfigHelper.h"

@implementation ConfigHelper


- (id)init {
    if (self = [super init]) {
    }
    return self;
}


+ (ConfigHelper*)defaultMercadoPagoConfig{
    
    
    //defaultMercadoPagoConfig Devuelve Singleton
    static ConfigHelper *conf = nil;
    static dispatch_once_t onlyOnce;
    dispatch_once(&onlyOnce,^{
        conf = [[self alloc] init];
    });
    
    //Obtenemos Datos de AppConfig.plist
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"AppConfig" ofType:@"plist"]];
    
    //Reviamos que El archivo y las llaves esten OK, Log de NO ser así
    if(!dictionary){
        [EepLog ErrorLog:@"PLIST AppConfig NO ha sido Cargado!"];
        //quizás abort es algo brusco...
        //abort();
    }else{
        [EepLog InfoLog:@"PLIST Cargado: %@",dictionary];
        
        conf.MPBaseUrl = [dictionary valueForKey:@"MPBaseURL"];
        conf.clientKey = [dictionary valueForKey:@"MPClientKey"];
        
        if (![conf MPBaseUrl]) {
            [EepLog ErrorLog:@"Archivo de configuración NO ha encontrado llave MPBaseURL"];
            //abort();
        }
        if(![conf clientKey]){
            [EepLog ErrorLog:@"Archvio de Configuración NO ha encontrado llave clientKey"];
            //abort();
        }
    }
    return conf;
    
}

+ (NSURL*)urlForService:(NSString*)service WithParams:(NSDictionary*)parameters;{
    
    NSMutableString *base = [NSMutableString new];
    [base appendString:[ConfigHelper defaultMercadoPagoConfig].MPBaseUrl];
    [base appendString:service];
    [base appendString:[NSString stringWithFormat:@"?public_key=%@",[ConfigHelper defaultMercadoPagoConfig].clientKey]];
    if(parameters){
        for(NSString *key in parameters){
            NSString *value = [parameters valueForKey:key];
            [base appendString:[NSString stringWithFormat:@"&%@=%@",key,value]];
        }
    }
    
    [EepLog InfoLog:@"%s -> %@",__PRETTY_FUNCTION__,base];
    NSURL *url = [NSURL URLWithString:[base stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    return url;
    
}

+ (NSURL*)urlForServices:(NSString*)service withMethod:(NSString*)method withParameters:(NSDictionary*)parameters{
    
    
    NSMutableString *base = [NSMutableString new];
    [base appendString:[ConfigHelper defaultMercadoPagoConfig].MPBaseUrl];
    [base appendString:service];
    [base appendString:[NSString stringWithFormat:@"/%@",method]];
    [base appendString:[NSString stringWithFormat:@"?public_key=%@",[ConfigHelper defaultMercadoPagoConfig].clientKey]];
    if(parameters){
        for(NSString *key in parameters){
            NSString *value = [parameters valueForKey:key];
            [base appendString:[NSString stringWithFormat:@"&%@=%@",key,value]];
        }
    }
    
    [EepLog InfoLog:@"%s -> %@",__PRETTY_FUNCTION__,base];
    NSURL *url = [NSURL URLWithString:[base stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    //stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    return url;
}

@end
