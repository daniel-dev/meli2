//
//  ConfigTest.m
//  MeLi2Tests
//
//  Created by Daniel Rodriguez on 18-06-18.
//  Copyright © 2018 Daniel Rodriguez. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ConfigHelper.h"

@interface ConfigTest : XCTestCase

@end

@implementation ConfigTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


- (void)testGetURLForServiceWithoutParams{
    NSString *expUrl = @"https://api.mercadopago.com/v1/OOOP?public_key=444a9ef5%E2%80%908a6b%E2%80%90429f%E2%80%90abdf%E2%80%90587639155d88";
    NSURL *urlTest = [ConfigHelper urlForService:@"OOOP" WithParams:nil];
    // urlParaServicio:@"OOOP" ConParametros:nil];
    NSString *rulStr = urlTest.absoluteString;
    XCTAssertEqualObjects(expUrl, rulStr,@"EEEP");
    
}

- (void)testGetUrlForServiceWithParameters{
    
    NSString *expUrl = @"https://api.mercadopago.com/v1/OOOP?public_key=444a9ef5%E2%80%908a6b%E2%80%90429f%E2%80%90abdf%E2%80%90587639155d88&KEY=VALUE";
    NSURL *urlTest = [ConfigHelper urlForService:@"OOOP" WithParams:@{@"KEY":@"VALUE"}];
    
    NSString *rulStr = urlTest.absoluteString;
    XCTAssertEqualObjects(expUrl, rulStr,@"EEEP");
    
    
}


@end
